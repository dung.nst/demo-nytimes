# Demo New York Times

> This project use `@vue/cli` 3.0.

## Quickstart

``` sh
npm install -g @vue/cli
npm install
npm install node-sass

# run development
npm run serve

# or deploy production
npm run build
```
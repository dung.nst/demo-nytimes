import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      redirect: 'home'
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/pages/home'),
      meta: {
        layout: 'default',
        title: 'Home'
      }
    },
    {
      path: '/article/:item',
      name: 'article.show',
      component: () => import('@/pages/detail'),
      meta: {
        layout: 'default',
        title: 'Article Detail'
      }
    }
  ]
})

export default router

let domain = 'https://api.nytimes.com/svc/'

let header = {
  'api-key': 'dad650aeeb3645dcae870891203b8647'
}

// 'q': 'singapore',
//   'page': 0

let api = {
  getArticle: domain + 'search/v2/articlesearch.json',
  getTopStories: domain + 'topstories/v2/home.json'
}

export default {
  header: header,
  api: api
}

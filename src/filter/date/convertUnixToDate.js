import moment from 'moment'

const convertUnixToDate = function (date, format) {
  return moment(date).format(format)
}

export default convertUnixToDate

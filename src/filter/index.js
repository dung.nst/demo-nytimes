import convertUnixToDate from './date/convertUnixToDate'

export default {
  install (Vue) {
    Vue.filter('convertUnixToDate', convertUnixToDate)
  }
}

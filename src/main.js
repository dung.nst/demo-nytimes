import Vue from 'vue'
import App from './app'
import router from './router'
import filters from './filter'

import GlobalComponents from './config/global-components'
import { VueExtendLayout, layout } from 'vue-extend-layout'
import BootstrapVue from 'bootstrap-vue'


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'font-awesome/css/font-awesome.min.css'
import './assets/scss/main.scss'


Vue.use(GlobalComponents)
Vue.use(VueExtendLayout)
Vue.use(BootstrapVue)
Vue.use(filters)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  ...layout
}).$mount('#app')

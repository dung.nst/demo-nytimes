import { Model, Collection } from 'vue-mc'
import axios from 'axios'
import endpoint from '@/const/endpoint'

export class ArticleModel extends Model {
}

/**
 * Article collection
 */
export class ArticleList extends Collection {
  model () {
    return ArticleModel
  }

  defaults () {
    return {
      orderBy: 'title'
    }
  }

  async fetch () {
    const headers = {
      params: endpoint.header
    }
    const snapshot = await axios.get(endpoint.api.getTopStories, headers)
    return snapshot.data.results
  }

  async fetchById (uid) {
    // const snapshot = await fb.blogArticleRef.child(uid).once('value')
    // return snapshot.val()
  }
}

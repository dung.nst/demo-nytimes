import HeaderComponent from '@/components/header.vue'
import TopHeaderComponent from '@/components/top-header.vue'
import NavigationComponent from '@/components/navigation.vue'
import FooterComponent from '@/components/footer.vue'

import LatestArticleComponent from '@/components/latest-article.vue'

const GlobalComponents = {
  install (Vue) {
    Vue.component('header-component', HeaderComponent)
    Vue.component('top-header', TopHeaderComponent)
    Vue.component('navigation', NavigationComponent)
    Vue.component('footer-component', FooterComponent)
    Vue.component('latest-article', LatestArticleComponent)
  }
}

export default GlobalComponents
